import {MigrationInterface, QueryRunner, Table} from "typeorm";

export default class CreateStudent1620350453309 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: "student",
                columns: [
                    {
                        name: "user",
                        type: "varchar",
                        isPrimary: true
                    },
                    {
                        name: "firstName",
                        type: "varchar"
                    },
                    {
                        name: "lastName",
                        type: "varchar"
                    },
                    {
                        name: "schoolName",
                        type: "varchar"
                    },
                    {
                        name: "teacher",
                        type: "varchar"
                    },
                    {
                        name: "class",
                        type: "varchar"
                    },
                    {
                        name: "password",
                        type: "varchar"
                    },
                    {
                        name: "created_At",
                        type: "timestamp",
                        default: "now()"
                    },
                    {
                        name: "updated_At",
                        type: "timestamp",
                        default: "now()"
                    }
                ]
            })
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        queryRunner.dropTable('student')
    }

}
