import { Router } from 'express';
import questionRouter from './question.routes';
import studentRouter from './student.routes';

const routes = Router()

routes.use('/student', studentRouter)

routes.use('/question', questionRouter)

export default routes;