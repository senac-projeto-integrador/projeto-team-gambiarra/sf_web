import {Column, CreateDateColumn, Entity, JoinTable, ManyToMany, PrimaryColumn, UpdateDateColumn} from "typeorm";
import Question from "./Question";

@Entity()
export default class Student {

    @PrimaryColumn({
        length: 20,
        unique: true
    })
    user: string;

    @Column({
        length: 20
    })
    firstName: string

    @Column({
        length: 100
    })
    lastName: string

    @Column({
        length: 50
    })
    schoolName: string

    @Column({
        length: 20
    })
    teacher: string

    @Column({
        length: 10
    })
    class: string

    @Column({
        length: 40
    })
    password: string

    @ManyToMany( type => Question, {eager: true})
    @JoinTable()
    questions: Question[]

    @CreateDateColumn({name: "created_At"})
    createdAt: Date

    @UpdateDateColumn({name: "updated_At"})
    updatedAt: Date

}
